import 'package:flutter/material.dart';
import 'package:wisatakediri/main_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Wisata Bandung',
      theme: ThemeData(),
      home: const MainScreen(),
    );
  }
}


// class FirstScreen extends StatefulWidget {
//   const FirstScreen({super.key});
 
//   @override
//   State<FirstScreen> createState() => _FirstScreenState();
// }
 
// class _FirstScreenState1 extends State<FirstScreen> {
//   String? language;
 
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: DropdownButton<String>(
//         items: const <DropdownMenuItem<String>>[
//           DropdownMenuItem<String>(
//             value: 'Dart',
//             child: Text('Dart'),
//           ),
//           DropdownMenuItem<String>(
//             value: 'Kotlin',
//             child: Text('Kotlin'),
//           ),
//           DropdownMenuItem<String>(
//             value: 'Swift',
//             child: Text('Swift'),
//           ),
//         ],
//         value: language,
//         hint: const Text('Select Language'),
//         onChanged: (String? value) {
//           setState(() {
//             language = value;
//           });
//         },
//       ),
//     );
//   }
// }

// class _FirstScreenState2 extends State<FirstScreen> {
//   String _name = '';
 
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Padding(
//         padding: const EdgeInsets.all(16.0),
//         child: Column(
//           children: [
//             TextField(
//               decoration: const InputDecoration(
//                 hintText: 'Write your name here...',
//                 labelText: 'Your Name',
//               ),
//               onChanged: (String value) {
//                 setState(() {
//                   _name = value;
//                 });
//               },
//             ),
//             const SizedBox(height: 20),
//             ElevatedButton(
//               child: const Text('Submit'),
//               onPressed: () {
//                 showDialog(
//                   context: context,
//                   builder: (context) {
//                     return AlertDialog(
//                       content: Text('Hello, $_name'),
//                     );
//                   }
//                 );
//               },
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }

// class _FirstScreenState3 extends State<FirstScreen> {
//   bool lightOn = false;
 
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Switch(
//         value: lightOn,
//         onChanged: (bool value) {
//           setState(() {
//             lightOn = value;
//           });
 
//           ScaffoldMessenger.of(context).showSnackBar(
//             SnackBar(
//               content: Text(lightOn ? 'Light On' : 'Light Off'),
//               duration: const Duration(seconds: 1),
//             ),
//           );
//         },
//       ),
//     );
//   }
// }

// class _FirstScreenState4 extends State<FirstScreen> {
//   String? language;
 
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: Column(
//         mainAxisSize: MainAxisSize.min,
//         children: <Widget>[
//           ListTile(
//             leading: Radio<String>(
//               value: 'Dart',
//               groupValue: language,
//               onChanged: (String? value) {
//                 setState(() {
//                   language = value;
//                   showSnackbar();
//                 });
//               },
//             ),
//             title: const Text('Dart'),
//           ),
//           ListTile(
//             leading: Radio<String>(
//               value: 'Kotlin',
//               groupValue: language,
//               onChanged: (String? value) {
//                 setState(() {
//                   language = value;
//                   showSnackbar();
//                 });
//               },
//             ),
//             title: const Text('Kotlin'),
//           ),
//           ListTile(
//             leading: Radio<String>(
//               value: 'Swift',
//               groupValue: language,
//               onChanged: (String? value) {
//                 setState(() {
//                   language = value;
//                   showSnackbar();
//                 });
//               },
//             ),
//             title: const Text('Swift'),
//           ),
//         ],
//       ),
//     );
//   }
 
//   void showSnackbar() {
//     ScaffoldMessenger.of(context).showSnackBar(
//       SnackBar(
//         content: Text('$language selected'),
//         duration: const Duration(seconds: 1),
//       ),
//     );
//   }
// }

// class _FirstScreenState extends State<FirstScreen> {
//   bool agree = false;
 
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('First Screen'),
//       ),
//       body: ListTile(
//         leading: Checkbox(
//           value: agree,
//           onChanged: (bool? value) {
//             setState(() {
//               agree = value!;
//             });

//             showSnackbar();
//           },
//         ),
//         title: const Text('Agree / Disagree'),
//       ),
//     );
//   }

//   void showSnackbar() {
//     ScaffoldMessenger.of(context).showSnackBar(
//       SnackBar(
//         content: Text('$agree selected'),
//         duration: const Duration(seconds: 1),
//       ),
//     );
//   }
// }